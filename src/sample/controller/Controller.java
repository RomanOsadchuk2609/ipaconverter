package sample.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.*;
import sample.model.*;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.Destination;
import javax.print.attribute.standard.Sides;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;
import java.awt.*;
import java.io.*;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    @FXML
    private AnchorPane backgroundPain;

    @FXML
    private Button btnChooseFile;

    @FXML
    private Label labelFileName;
    @FXML
    private TableView<SoundAndAmount> tableView;

    @FXML
    private MenuItem saveAsHtmlMI, saveAsDocxMI, saveAsPdfMI, saveAsMI, saveWithTextMI, printMI, exitMI, enUsMI;

    @FXML
    private TableColumn<SoundAndAmount, String> soundColumn;

    @FXML
    private TableColumn<SoundAndAmount, Long> amountColumn;

    @FXML
    private AnchorPane wordParsingPane;

    @FXML
    private Label wordParsingLabel;
    @FXML
    private TextArea textFromFileTA;

    private int parsedWords=0;
    private FileChooser fileChooser, fileSaverHTML, fileSaverDOCX, fileSaverPDF;
    private String fileName="";
    private Map<String, String> dictionaryEnUs = new HashMap<>();
    private AmountOfSounds amountOfSounds = new AmountOfSounds();
    private DictionarySetUp dictionarySetUpEnUs = new DictionarySetUp(Language.ENGLISH_US);
    private CountSounds countSounds = new CountSounds();
    private String parsedText="";

    private AnimationTimer at = new AnimationTimer() {
        long lastUpdate = 0;
        @Override
        public void handle(long now) {
            if (now - lastUpdate >1_000_000){
                if (countSounds.isAlive()){
                    backgroundPain.setDisable(true);
                    wordParsingLabel.setText("Оброблено слів: "+parsedWords);
                }
                else {
                    wordParsingLabel.setText("Оброблено слів: "+parsedWords);
                    tableView.setItems(amountOfSounds.getData());
                    labelFileName.setText(fileName);
                    textFromFileTA.setText(parsedText);
                    wordParsingPane.setVisible(false);
                    backgroundPain.setDisable(false);

                    //stasyan's part
                    //saveResultToTable();
                    //
                    this.stop();
                }
            }
            lastUpdate = now;
        }
    };

    @FXML
    void initialize(){
        //initMenuIcons();
        saveAsHtmlMI.setVisible(false);
        dictionarySetUpEnUs.start();

        labelFileName.setText("");

        fileChooser = new FileChooser();
        fileChooser.setTitle("Оберіть текстовий файл");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        FileChooser.ExtensionFilter textFilter = new FileChooser.ExtensionFilter("Text files", "*.txt", "*.docx", "*.doc");
        fileChooser.getExtensionFilters().add(textFilter);
        fileChooser.setSelectedExtensionFilter(textFilter);

        fileSaverHTML = new FileChooser();
        fileSaverHTML.setTitle("Введіть назву файла для збереження результату");
        fileSaverHTML.setInitialFileName(".html");
        fileSaverHTML.setInitialDirectory(new File(System.getProperty("user.home")));
        FileChooser.ExtensionFilter htmlFilter = new FileChooser.ExtensionFilter("Html file", "*.html");
        fileSaverHTML.getExtensionFilters().add(htmlFilter);
        fileSaverHTML.setSelectedExtensionFilter(htmlFilter);


        fileSaverDOCX = new FileChooser();
        fileSaverDOCX.setTitle("Введіть назву файла для збереження результату");
        fileSaverDOCX.setInitialFileName(".docx");
        fileSaverDOCX.setInitialDirectory(new File(System.getProperty("user.home")));
        FileChooser.ExtensionFilter docxFile = new FileChooser.ExtensionFilter("DOCX file", "*.docx");
        fileSaverDOCX.getExtensionFilters().add(docxFile);
        fileSaverDOCX.setSelectedExtensionFilter(docxFile);

        fileSaverPDF = new FileChooser();
        fileSaverPDF.setTitle("Введіть назву файла для збереження результату");
        fileSaverPDF.setInitialFileName(".pdf");
        fileSaverPDF.setInitialDirectory(new File(System.getProperty("user.home")));
        FileChooser.ExtensionFilter pdfFile = new FileChooser.ExtensionFilter("PDF file", "*.pdf");
        fileSaverPDF.getExtensionFilters().add(pdfFile);
        fileSaverPDF.setSelectedExtensionFilter(pdfFile);

        amountColumn.setCellValueFactory(new PropertyValueFactory<SoundAndAmount, Long>("amount"));
        soundColumn.setCellValueFactory(new PropertyValueFactory<SoundAndAmount, String>("sound"));

        tableView.setItems(amountOfSounds.getData());

    }

    @FXML
    void onClickEnUsMI(ActionEvent event) {

    }

    @FXML
    void onClickExitMI(ActionEvent event) {
        at.stop();
        System.exit(0);
    }

    @FXML
    void onClickPrintMI(ActionEvent event) {
        //pageSetup(tableView,(Stage)btnChooseFile.getScene().getWindow());
        printFile();
    }

    @FXML
    void onClickSaveAsHtmlMI(ActionEvent event) {
        File file = fileSaverHTML.showSaveDialog(btnChooseFile.getScene().getWindow());
        if (file!=null){
            SavingResultAsHtml SavingResultAsHtml = new SavingResultAsHtml(file);
            SavingResultAsHtml.run();
        }
    }

    @FXML
    void onClickSaveAsPdfMI(ActionEvent event) {
        File file = fileSaverPDF.showSaveDialog(btnChooseFile.getScene().getWindow());
        if (file!=null){
            SavingResultAsPdf savingResultAsPdf = new SavingResultAsPdf(file);
            savingResultAsPdf.run();
        }
    }

    @FXML
    void onClickSaveAsDocxMI(ActionEvent event) {
        File file = fileSaverDOCX.showSaveDialog(btnChooseFile.getScene().getWindow());
        if (file!=null){
            SavingResultAsDocx savingResultAsDocx = new SavingResultAsDocx(file);
            savingResultAsDocx.run();
        }
    }

    @FXML
    void onClickSaveWithTextMI(ActionEvent event) {
        File file = fileSaverDOCX.showSaveDialog(btnChooseFile.getScene().getWindow());
        if (file!=null){
            SavingResultAsDocx savingResultAsDocx = new SavingResultAsDocx(file, parsedText+"\n");
            savingResultAsDocx.run();
        }
    }

    @FXML
    void onCLickBtnChooseFile(ActionEvent event) {

        File file = fileChooser.showOpenDialog(btnChooseFile.getScene().getWindow());
        if (file != null) {
            System.out.println("Opened file: "+file.getAbsolutePath());
            fileName = file.getName();
            labelFileName.setText(fileName);
            countSounds = new CountSounds();
            countSounds.setPath(file.getAbsolutePath());
            countSounds.start();
            wordParsingPane.setVisible(true);
            textFromFileTA.setText("");
        }
            /*BufferedReader reader;
            try {
                reader = new BufferedReader(new FileReader("src/sample/ipa/textDictionary/en_us.txt"));
                String line = reader.readLine();
                int i=0;
                while (line != null && i<20) {
                    if (!line.contains("XXXXX")){
                        String word="", transcription="";
                        word = line.substring(0,line.indexOf('\t'));
                        transcription = line.substring(line.indexOf('/')+1,line.lastIndexOf('/'));
                        if (transcription.contains("/")){
                            transcription = transcription.substring(0,transcription.indexOf("/"));
                        }
                        System.out.println(line+"|word: "+word+"; transcription: "+transcription+".");
                        dictionaryEnUs.put(word,transcription);
                    }
                    line = reader.readLine();
                }
                reader.close();
                FileOutputStream fileOut = new FileOutputStream("src/sample/ipa/mapDictionary/map_en_us.txt");
                ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
                objectOut.writeObject(dictionaryEnUs);
                objectOut.close();
                System.out.println(dictionaryEnUs.get("java"));
            } catch (IOException e) {
                e.printStackTrace();
            }*/

    }

    private class SavingResultAsHtml extends Thread{

        File file;

        public SavingResultAsHtml(File file) {
            this.file = file;
        }

        @Override
        public void run() {
            saveResultToTable(file);
        }

    }

    private class SavingResultAsDocx extends Thread{

        File file;
        String text="";

        public SavingResultAsDocx(File file) {
            this.file = file;
        }

        public SavingResultAsDocx(File file, String text) {
            this.file = file;
            this.text = text;
        }

        @Override
        public void run() {
            try {
                saveResultToDocxFile(file,text);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private class SavingResultAsPdf extends Thread{

        File file;

        public SavingResultAsPdf(File file) {
            this.file = file;
        }

        @Override
        public void run() {
            saveResultToPdfFile(file);
        }

    }


    private class DictionarySetUp extends Thread{
        Language language;

        public DictionarySetUp(Language language) {
            this.language = language;
        }

        @Override
        public void run() {
            try {
                switch (language){
                    case ENGLISH_US:{
                        if (dictionaryEnUs.isEmpty()){
                            FileInputStream fileIn = new FileInputStream("map_en_us.txt");
                            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                            dictionaryEnUs = (Map) objectIn.readObject();
                            objectIn.close();
                            System.out.println(dictionaryEnUs.get("java"));
                        }
                        System.out.println("DICTIONARY SET UP FINISHED SUCCESSFULLY!");
                    }break;
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                dictionaryEnUs = new HashMap<>();
                System.out.println("DICTIONARY SET UP FAILED!");
            }
        }
    }

    private class CountSounds extends Thread{
        private String path;

        public CountSounds(String path) {
            this.path = path;
        }

        public CountSounds() {
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        @Override
        public void run() {
            parsedWords=0;
            parsedText="";
            at.start();
            System.out.println(new Date());
            BufferedReader reader;
            Pattern wordPattern = Pattern.compile("\\w+");
            try {
                boolean isFirstLine=true;
                if (path.endsWith(".docx")){
                    FileInputStream fis = new FileInputStream(path);

                    XWPFDocument document = new XWPFDocument(fis);

                    List<XWPFParagraph> paragraphs = document.getParagraphs();

                    System.out.println("Total no of paragraph "+paragraphs.size());
                    for (XWPFParagraph para : paragraphs) {
                        parseLine(wordPattern, para.getText());
                        if (isFirstLine){
                            isFirstLine = false;
                            parsedText += para.getText();
                        }else {
                            parsedText += "\n" + para.getText();
                        }
                    }
                    fis.close();
                }
                else if ((path.endsWith(".txt"))){
                    reader = new BufferedReader(new FileReader(path));
                    String line = reader.readLine();
                    amountOfSounds.reset();
                    while (line != null) {
                        parseLine(wordPattern,line);
                        if (isFirstLine){
                            isFirstLine = false;
                            parsedText +=line;
                        }else {
                            parsedText += "\n" + line;
                        }
                        line = reader.readLine();
                    }
                    reader.close();
                }
                else if ((path.endsWith(".doc"))){
                    FileInputStream fis = new FileInputStream(path);

                    HWPFDocument doc = new HWPFDocument(fis);

                    WordExtractor we = new WordExtractor(doc);

                    String[] paragraphs = we.getParagraphText();

                    System.out.println("Total no of paragraph "+paragraphs.length);
                    for (String para : paragraphs) {
                        parseLine(wordPattern,para);
                        if (isFirstLine){
                            isFirstLine = false;
                            parsedText += para;
                        }else {
                            parsedText += "\n" + para;
                        }
                    }
                    fis.close();
                }
                System.out.println(new Date());
                System.out.println("Amount of words = "+parsedWords);
            } catch (IOException e) {
                System.out.println(new Date());
                e.printStackTrace();
            }
        }

        private void parseLine(Pattern wordPattern, String line){
            Matcher m =wordPattern.matcher(line);
            while (m.find()) {
                System.out.println(m.group());
                String word = m.group();
                word = word.toLowerCase();
                parsedWords++;
                //amountOfSounds = TranscriptionApiEnUs.getAmountOfSounds(amountOfSounds,word);
                amountOfSounds = TranscriptionEnUs.getAmountOfSounds(amountOfSounds,word,dictionaryEnUs,Language.ENGLISH_US);
            }
        }
    }

    private void initMenuIcons(){
        Image iconSave = new Image(getClass().getResourceAsStream("../img/icon_save.png"));
        Image iconPrint = new Image(getClass().getResourceAsStream("../img/icon_print.png"));
        Image iconExit = new Image(getClass().getResourceAsStream("../img/icon_exit.png"));
        Image iconCheck = new Image(getClass().getResourceAsStream("../img/icon_check.png"));

        ImageView imageSave = new ImageView(iconSave);
        imageSave.setFitWidth(15);
        imageSave.setFitHeight(15);

        ImageView imagePrint = new ImageView(iconPrint);
        imagePrint.setFitWidth(15);
        imagePrint.setFitHeight(15);

        ImageView imageExit = new ImageView(iconExit);
        imageExit.setFitWidth(15);
        imageExit.setFitHeight(15);

        ImageView imageCheck = new ImageView(iconCheck);
        imageCheck.setFitWidth(15);
        imageCheck.setFitHeight(15);

        saveAsMI.setGraphic(imageSave);
        printMI.setGraphic(imagePrint);
        exitMI.setGraphic(imageExit);
        enUsMI.setGraphic(imageCheck);
    }

    private void saveResultToTable(File destinationFile){
        File htmlTemplateFile = new File("table_template.html");
        String htmlString = null;
        try {
            htmlString = FileUtils.readFileToString(htmlTemplateFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String title = fileName;
        if (htmlString != null) {
            htmlString = htmlString.replace("$title$", title);
            List<SoundAndAmount> list = amountOfSounds.getDataAsList();
            int i = 0;
            for (SoundAndAmount object: list) {
                String sound = "$"+i+"s$";
                String amount = "$"+i+"a$";

                htmlString = htmlString.replace(sound, object.getSound());
                htmlString = htmlString.replace(amount, object.getAmount()+"");

                i++;
            }
        }
        File newHtmlFile = new File(destinationFile.getAbsolutePath());
        try {
            FileUtils.writeStringToFile(newHtmlFile, htmlString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveResultToDocxFile(File destinationFile, String text) throws IOException {

        List<SoundAndAmount> list = amountOfSounds.getDataAsList();


        //Blank Document
        XWPFDocument document = new XWPFDocument();

        //Write the Document in file system
        FileOutputStream out = new FileOutputStream(new File(destinationFile.getAbsolutePath()));
        if (text!=null && !text.isEmpty()){
            //create Paragraph
            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun run = paragraph.createRun();
            run.setText(text);
        }
        //create table
        XWPFTable table = document.createTable();

        //create first row
        XWPFTableRow tableRowOne = table.getRow(0);

        tableRowOne.getCell(0).setText(  "Звук");
        tableRowOne.getCell(0).setWidth("3000");
        tableRowOne.addNewTableCell().setText("Кількість");
        tableRowOne.getCell(1).setWidth("3000");

        for (SoundAndAmount object: list) {

            //create other rows
            XWPFTableRow tableRowTwo = table.createRow();

            tableRowTwo.getCell(0).setText(object.getSound()+"\t\t\t");
            tableRowTwo.getCell(1).setText(object.getAmount()+"\t\t\t");
        }

        document.write(out);
        out.close();

        System.out.println("docx file written successully");

    }

    private void printFile(){
        /*saveResultToPdfFile();
        FileInputStream  is = null;
        try {
            is = new FileInputStream("table.pdf");

            DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
            PrintService service = PrintServiceLookup.lookupDefaultPrintService();
            System.out.println(service.getName());
            Doc doc = new SimpleDoc(is, flavor, null);

            PrintRequestAttributeSet attributes = new HashPrintRequestAttributeSet();
            attributes.add(new Copies(1));
            DocPrintJob printJob = service.createPrintJob();

            PrintJobWatcher pjw = new PrintJobWatcher(printJob);
            printJob.print(doc,attributes);
            pjw.waitForDone();
            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (PrintException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        prepareFileToPrint();
        File file = new File("table.txt");
        if (Desktop.isDesktopSupported()){
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.PRINT))
            {
                try {
                    desktop.print(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //System.exit(0);
            System.out.println("file was printed");
        }

    }

    private void prepareFileToPrint(){
        List<SoundAndAmount> list = amountOfSounds.getDataAsList();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("table.txt", "UTF-8");
            writer.println("Кількість звуків у тексті файла "+fileName+":");
            for (SoundAndAmount el : list){
                writer.println(el.getSound() + "\t-\t" + el.getAmount());
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    private void saveResultToPdfFile(File destinationFile){
        BaseFont bf = null;
        try {
            bf = BaseFont.createFont(
                    "c://windows/fonts/arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new com.itextpdf.text.Font(bf, 12);Document document = new Document();
            PdfPTable table = new PdfPTable(new float[] { 2, 2});
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            PdfPCell soundHeader = new PdfPCell(new Phrase("Звук", font));
            PdfPCell amountHeader = new PdfPCell(new Phrase("Кількість", font));
            soundHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
            amountHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(soundHeader);
            table.addCell(amountHeader);
            table.setHeaderRows(1);
            PdfPCell[] cells = table.getRow(0).getCells();
            List<SoundAndAmount> list = amountOfSounds.getDataAsList();
            for (int j=0;j<cells.length;j++){
                cells[j].setBackgroundColor(BaseColor.LIGHT_GRAY);
            }
            for (SoundAndAmount el : list){
                PdfPCell sound = new PdfPCell(new Phrase(el.getSound(), font));
                sound.setHorizontalAlignment(Element.ALIGN_CENTER);
                PdfPCell amount = new PdfPCell(new Phrase(el.getAmount()+"", font));
                amount.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(sound);
                table.addCell(amount);
            }
            PdfWriter.getInstance(document, new FileOutputStream(destinationFile));
            document.open();
            Paragraph fileNameParagraph = new Paragraph(new Phrase("Кількість звуків у тексті файла "+fileName+":\n ", font));
            fileNameParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(new Paragraph(fileNameParagraph));
            document.add(table);
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    private void pageSetup(Node node, Stage owner)
    {
        // Create the PrinterJob
        PrinterJob job = PrinterJob.createPrinterJob();

        if (job == null){
            return;
        }
        // Show the page setup dialog
        boolean proceed = job.showPageSetupDialog(owner);

        if (proceed) {
            print(job, node);
        }
    }

    private void print(PrinterJob job, Node node)
    {        // Print the node
        TableView tableView = (TableView)node;
        tableView.setPrefHeight(1090);
        boolean printed = job.printPage(tableView);
        tableView.setPrefHeight(520);
        if (printed) {
            job.endJob();
        }
    }
    class PrintJobWatcher {
        boolean done = false;

        PrintJobWatcher(DocPrintJob job) {
            job.addPrintJobListener(new PrintJobAdapter() {
                public void printJobCanceled(PrintJobEvent pje) {
                    System.out.println("printJobCanceled");
                    allDone();
                }
                public void printJobCompleted(PrintJobEvent pje) {
                    System.out.println("printJobCompleted");
                    allDone();
                }
                public void printJobFailed(PrintJobEvent pje) {
                    System.out.println("printJobFailed");
                    allDone();
                }
                public void printJobNoMoreEvents(PrintJobEvent pje) {
                    System.out.println("printJobNoMoreEvents");
                    allDone();
                }
                void allDone() {
                    synchronized (PrintJobWatcher.this) {
                        done = true;
                        System.out.println("Printing done ...");
                        PrintJobWatcher.this.notify();
                    }
                }
            });
        }
        public synchronized void waitForDone() {
            try {
                while (!done) {
                    wait();
                }
            } catch (InterruptedException e) {
            }
        }
    }


}
