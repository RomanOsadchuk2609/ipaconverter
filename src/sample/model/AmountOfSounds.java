package sample.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.ipa.ipaSigns.PhoneticSignsEnUs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AmountOfSounds {
    public Map<String, Long> amountOfDiphthongs = new HashMap<>();
    public Map<String, Long> amountOfVowels = new HashMap<>();
    public Map<String, Long> amountOfConsonants = new HashMap<>();

    public AmountOfSounds() {
        reset();
    }

    public void reset(){

        for (String sound: PhoneticSignsEnUs.diphthongs) {
            amountOfDiphthongs.put(sound,0l);
        }

        for (String sound: PhoneticSignsEnUs.vowels) {
            amountOfVowels.put(sound,0l);
        }

        for (String sound: PhoneticSignsEnUs.consonants) {
            amountOfConsonants.put(sound,0l);
        }
    }

    public ObservableList<SoundAndAmount> getData(){
        List<SoundAndAmount> list = new ArrayList<>();
        for (String sound: PhoneticSignsEnUs.diphthongs) {
            list.add(new SoundAndAmount(sound,amountOfDiphthongs.get(sound)));
        }

        for (String sound: PhoneticSignsEnUs.vowels) {
            String longSounds="ɑiɔuɝɜ", soundInTable=sound;
            if (longSounds.contains(sound)) {
                soundInTable+=":";
            }
            if (soundInTable.contains("ɝ:")){
                soundInTable = "ɜ:";
            }
            list.add(new SoundAndAmount(soundInTable,amountOfVowels.get(sound)));
        }

        for (String sound: PhoneticSignsEnUs.consonants) {
            String soundInTable = sound;
            if (soundInTable.equals("ɫ")){
                soundInTable="l";
            }
            else if (soundInTable.equals("ɹ")){
                soundInTable="r";
            }
            list.add(new SoundAndAmount(soundInTable,amountOfConsonants.get(sound)));
        }
        ObservableList<SoundAndAmount> data = FXCollections.observableArrayList(list);
        return data;
    }

    public List<SoundAndAmount> getDataAsList(){
        List<SoundAndAmount> list = new ArrayList<>();
        for (String sound: PhoneticSignsEnUs.diphthongs) {
            list.add(new SoundAndAmount(sound,amountOfDiphthongs.get(sound)));
        }

        for (String sound: PhoneticSignsEnUs.vowels) {
            String longSounds="ɑiɔuɝɜ", soundInTable=sound;
            if (longSounds.contains(sound)) {
                soundInTable+=":";
            }
            if (soundInTable.contains("ɝ:")){
                soundInTable = "ɜ:";
            }
            list.add(new SoundAndAmount(soundInTable,amountOfVowels.get(sound)));
        }

        for (String sound: PhoneticSignsEnUs.consonants) {
            String soundInTable = sound;
            if (soundInTable.equals("ɫ")){
                soundInTable="l";
            }
            else if (soundInTable.equals("ɹ")){
                soundInTable="r";
            }
            list.add(new SoundAndAmount(soundInTable,amountOfConsonants.get(sound)));
        }
        return list;
    }
}
