package sample.model;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import sample.ipa.ipaSigns.PhoneticSignsEnUs;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class TranscriptionApiEnUs {
    public static void main(String[] args) {
        TranscriptionApiEnUs helper = new TranscriptionApiEnUs();
        try {
            helper.executeRequest("java");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static AmountOfSounds getAmountOfSounds(AmountOfSounds amountOfSounds, String word){
        try {
            String response=executeRequest(word);
            if (!response.isEmpty()){
                String transcription = getTranscription(response);
                if (!transcription.isEmpty()){


                    for (String sound: PhoneticSignsEnUs.diphthongs){
                        long amount = countSubStrings(transcription,sound);
                        if (amount>0){
                            amount += amountOfSounds.amountOfDiphthongs.get(sound);
                            amountOfSounds.amountOfDiphthongs.replace(sound,amount);
                            transcription = transcription.replaceAll(sound,"");
                        }
                    }

                    for (String sound: PhoneticSignsEnUs.vowels){
                        String apiSound = sound;
                        if (sound.equals("ɝ")) apiSound = "ɜ";
                        long amount = countSubStrings(transcription,apiSound);
                        if (amount>0){
                            amount += amountOfSounds.amountOfVowels.get(sound);
                            amountOfSounds.amountOfVowels.replace(sound,amount);
                            transcription = transcription.replaceAll(apiSound,"");
                        }
                    }

                    for (String sound: PhoneticSignsEnUs.consonants){
                        String apiSound = sound;
                        if (sound.equals("ɹ")) apiSound = "r";
                        else if (sound.equals("ɫ")) apiSound = "l";
                        long amount = countSubStrings(transcription,apiSound);
                        if (amount>0){
                            amount += amountOfSounds.amountOfConsonants.get(sound);
                            amountOfSounds.amountOfConsonants.replace(sound,amount);
                            transcription = transcription.replaceAll(apiSound,"");
                        }
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return amountOfSounds;
    }

    public static String executeRequest(String word) throws IOException, URISyntaxException {
        String https_url = "https://wordsapiv1.p.mashape.com/words/"+word+"/pronunciation";
        URL url = new URL(https_url);
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request= new HttpGet(https_url);
        request.setHeader("X-Mashape-Key","UXvlQmpJKtmshmvuBBZi3CtiAPGjp16jgFgjsngcjmsNM4eFFF");
        request.setHeader("Accept","application/json");
        HttpResponse response = client.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
       /* System.out.println("Sending 'GET' request to URL: " + url);
        System.out.println("Response Code: " + responseCode);*/
        if (responseCode == 200) {
            return EntityUtils.toString(response.getEntity());
        }
        else {
            return "";
        }
    }

    private static String getTranscription(String response){
        if (response.contains("all")){
            return response.substring(response.lastIndexOf(':')+2,response.lastIndexOf('\"'));
        }
        else return "";
    }

    private static long countSubStrings(String transcription, String substring){

        long i=0l;
        while (transcription.contains(substring)) {
            i++;
            transcription = transcription.replaceFirst(substring,"");
        }
        return i;
    }

}
