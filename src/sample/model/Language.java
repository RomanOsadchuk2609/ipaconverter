package sample.model;

public enum Language {
    ENGLISH_US, ENGLISH_UK
}
