package sample.model;

public class SoundAndAmount {
    private String sound;
    private long amount;

    public SoundAndAmount(String sound, long amount) {
        this.sound = sound;
        this.amount = amount;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
