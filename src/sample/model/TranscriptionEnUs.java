package sample.model;

import sample.ipa.ipaSigns.PhoneticSignsEnUs;

import java.util.Map;

public class TranscriptionEnUs {
    public static AmountOfSounds getAmountOfSounds(AmountOfSounds amountOfSounds, String word, Map<String, String> dictionary, Language language){

        switch (language){
            case ENGLISH_US:{
                if (dictionary.containsKey(word)){
                    String transcription = dictionary.get(word);
                    for (String sound: PhoneticSignsEnUs.diphthongs){
                        long amount = countSubStrings(transcription,sound);
                        if (amount>0){
                            amount += amountOfSounds.amountOfDiphthongs.get(sound);
                            amountOfSounds.amountOfDiphthongs.replace(sound,amount);
                            transcription = transcription.replaceAll(sound,"");
                        }
                    }

                    for (String sound: PhoneticSignsEnUs.vowels){
                        long amount = countSubStrings(transcription,sound);
                        if (amount>0){
                            amount += amountOfSounds.amountOfVowels.get(sound);
                            amountOfSounds.amountOfVowels.replace(sound,amount);
                            transcription = transcription.replaceAll(sound,"");
                        }
                    }

                    for (String sound: PhoneticSignsEnUs.consonants){
                        long amount = countSubStrings(transcription,sound);
                        if (amount>0){
                            amount += amountOfSounds.amountOfConsonants.get(sound);
                            amountOfSounds.amountOfConsonants.replace(sound,amount);
                            transcription = transcription.replaceAll(sound,"");
                        }
                    }
                }
                else {
                    amountOfSounds = TranscriptionApiEnUs.getAmountOfSounds(amountOfSounds,word);
                }
            }break;
        }

        return amountOfSounds;
    }

    private static long countSubStrings( String transcription, String substring){
        long i=0l;
        while (transcription.contains(substring)) {
            i++;
            transcription = transcription.replaceFirst(substring,"");
        }
        return i;
    }
}
