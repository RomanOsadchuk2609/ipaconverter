package sample.ipa.ipaSigns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PhoneticSignsEnUs {
    //ɑ = ɑ:
    //i = i:
    //ɔ = ɔ:
    //u = u:
    //ɝ = ɜ:
    //ɜ = ɜ:


    //local - api
    //ɝ - ɜ
    //ɹ - r
    //ɫ - l


    public static final List<String> vowels = new ArrayList<>(Arrays.asList(
            "æ","ɑ","i","ɪ","ɛ","ɒ",
            "ɔ","ʊ","u","ʌ","ɝ","ə"
    ));

    public static final List<String> diphthongs = new ArrayList<>(Arrays.asList(
            "eɪ","aɪ","aʊ","oʊ","ɔɪ","ɪə","ʊə","eə"
    ));

    public static final List<String> consonants = new ArrayList<>(Arrays.asList(
            "tʃ","dʒ","p","f","t","θ","s","k",
            "b","v","d","ð","z","ʒ","ɡ","ʃ",
            "h","m","n","ŋ","ɹ","ɫ","w","j"
    ));
}
